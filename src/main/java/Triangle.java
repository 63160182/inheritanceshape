/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Triangle extends Shape{
    private double b;
    private double h;
    public Triangle(double b,double h){
        super(b,h);
        this.b=b;
        this.h=h;
        System.out.println("Triangle created");
    }
    
    @Override
    public double calArea(){
        return 0.5*b*h;
    }
    
    @Override
    public void print(){
        System.out.println("Triangle = "+calArea()+" Base = "+b+" Height = "+h);
    }
    
}
