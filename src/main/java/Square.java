/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Square extends Rectangle{
    private double s;
    public Square(double s){
        super(s,s);
        this.s=s;
        System.out.println("Square created");
    }
    public double calArea(){
        return s*s;
    }
    
    public void print(){
        System.out.println("Square = "+calArea()+" Side = "+s);
    }
}
