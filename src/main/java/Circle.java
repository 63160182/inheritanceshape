/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Circle extends Shape{
    private double p=22/7;
    private double r;
    public Circle(double r){
        super(r,r);
        this.r=r;
        System.out.println("Circle created");
    }
    @Override
    public double calArea(){
        return p*r*r;
    }
    @Override
    public void print(){
        System.out.println("Circle = "+calArea()+" Radius = "+r);
    }
}
