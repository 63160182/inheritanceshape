/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Shape {
    protected double x=0;
    protected double y=0;
    public Shape(double x,double y){
        this.x=x;
        this.y=y;
        System.out.println("Shape created");
    }
    public double calArea(){
        return x*y;
    }
    public void print(){
        System.out.println("Shape = "+calArea()+" x = "+x+"y = "+y);
    }
}
