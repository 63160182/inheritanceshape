/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class TestShape {
    public static void main(String[] args){
        Shape shape=new Shape(9,9);
        shape.calArea();
        shape.print();
        
        Circle circle=new Circle(8);
        circle.calArea();
        circle.print();
        
        Rectangle rectangle = new Rectangle(7,7);
        rectangle.calArea();
        rectangle.print();
        
        Square square = new Square(6);
        square.calArea();
        square.print();
        
        Triangle triangle = new Triangle(5,5);
        triangle.calArea();
        triangle.print();
        
        System.out.println("Circle is a Shape: "+(circle instanceof Shape));
        System.out.println("Rectangle is a Shape: "+(rectangle instanceof Shape));
        System.out.println("Square is a Shape: "+(square instanceof Shape));
        System.out.println("Triangle is a Shape: "+(triangle instanceof Shape));
        
        Shape[] shapes = {circle,rectangle,square,triangle};
        for(int i=0;i<shapes.length; i++){
            shapes[i].calArea();
            shapes[i].print();

    }
}
