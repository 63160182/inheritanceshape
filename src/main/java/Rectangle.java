/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Rectangle extends Shape{
    protected double w;
    protected double h;
    public Rectangle(double w,double h){
        super(w,h);
        this.w=w;
        this.h=h;
        System.out.println("Rectangle created");
    }
    
    @Override
    public double calArea(){
        return w*h;
    }
    
    @Override
    public void print(){
        System.out.println("Rectangle = "+calArea()+" Width = "+w+" Height = "+h);
    }
}
